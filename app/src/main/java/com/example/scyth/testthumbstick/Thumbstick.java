package com.example.scyth.testthumbstick;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.support.constraint.solver.ArrayLinkedVariables;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

/**
 * TODO: document your custom view class.
 */
public class Thumbstick extends View {

    /**
     * All my listeners to be notified
     */
    private ArrayList<ThumbstickListener> listeners = new ArrayList<>();

    /**
     * Permanent background to the pointer
     */
    private ShapeDrawable backgroundCircle;

    /**
     * Semi-transparent background that represents the position of the a finger on the control.
     * The moving part of the thumbstick
     */
    private ShapeDrawable pointer;

    /**
     * Value, ranging from 0.0 to 1.0 representing where on the control
     * the thumbstick is currently at on the X axis
     */
    private float valueX;

    /**
     * Value, ranging from 0.0 to 1.0 representing where on the control
     * the thumbstick is currently at on the Y axis
     */
    private float valueY;

    /**
     * The default radius of the pointer circle
     */
    private int thumbstickCircleRadius = 75;

    public Thumbstick(Context context) {
        super(context);

        init();
    }

    public Thumbstick(Context c, AttributeSet attributeSet, int defStyle) {
        super(c, attributeSet,defStyle);

        init();
    }

    public Thumbstick(Context c, AttributeSet attributes) {
        super(c,attributes);

        init();
    }

    /**
     * Draws the background circle and the pointer circle
     * @param canvas
     */
    protected void onDraw(Canvas canvas) {
        backgroundCircle.draw(canvas);
        pointer.draw(canvas);
    }

    /**
     * The two shape drawables and their colors
     */
    private void init() {
        backgroundCircle = new ShapeDrawable(new OvalShape());
        backgroundCircle.getPaint().setColor(0xff74AC23);

        pointer = new ShapeDrawable(new OvalShape());
        backgroundCircle.getPaint().setColor(0xff0000ff);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * Lays out the background circle and thumb circle
     * @param changed
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        //we only care about updating if changes were made
        if(changed) {
            int x = 0;
            int y = 0;

            backgroundCircle.setBounds(x, y, x + getWidth(), y + getHeight());
            resetThumbCircle();
        }
    }

    /**
     * Moves the thumb circle back to the center of the control region
     */
    private void resetThumbCircle() {
        setThumbCircle(backgroundCircle.getBounds().centerX(), backgroundCircle.getBounds().centerY());
    }

    /**
     * Centers the pointer circle over the coordinates given. This method internally clamps the
     * X/Y values so that the pointer circle stays in side the control region.
     * @param x
     * @param y
     */
    private void setThumbCircle(int x, int y) {

        //set the value of input
        updateValues(x,y);

        //Clamp the X coordinate so that the pointer circle cannot
        //go off either the right or left side of the control
        if(x <= thumbstickCircleRadius)
            x = thumbstickCircleRadius;
        else if(x >= getWidth() - (thumbstickCircleRadius))
            x = getWidth() - thumbstickCircleRadius;


        //clamp the Y coordinate so that the pointer circle cannot go
        //off either the top or bottom of the control area
        if(y <= thumbstickCircleRadius)
            y = thumbstickCircleRadius;
        else if(y >= getHeight() - (thumbstickCircleRadius))
            y = getWidth() - thumbstickCircleRadius;



        //this moves the actual indicator for thumb position
        pointer.setBounds(x - thumbstickCircleRadius,       //LEFT
                y - thumbstickCircleRadius,                 //TOP
                x + thumbstickCircleRadius,                 //RIGHT
                y + thumbstickCircleRadius);                //BOTTOM

        invalidate();
    }

    /**
     * Moves the pointer circle to wherever the control is touched. When the pointer is released,
     * the pointer circle returns to the middle.
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch(event.getAction()) {
            //when the screen is touched, either statically or
            //dynamically, move the pointer circle to where the touch event happend
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_DOWN:
                setThumbCircle((int)event.getX(),(int)event.getY());
                return true;

            //If the screen is no longer being touched, reset to the middle!
            case MotionEvent.ACTION_UP:
                resetThumbCircle();
                return true;
        }

        return super.onTouchEvent(event);
    }

    /**
     * Takes in the coordinates of the thumbstick input and translates them into a number
     * @param xCoord Current lcoation of the thumbstick
     * @param yCoord Current location of the thumbstick
     */
    public void updateValues(int xCoord, int yCoord) {

        //get the width and height of the thumbstick area
        int width = getWidth();
        int height = getHeight();

        //X location of thumbstick relative to its center
        int relativeX = xCoord - getCenterY();

        //Y location of thumbstick relative to its center
        int relativeY = yCoord - getCenterY();

        //we want to calculate a decimal, 0.0-1.0, representing the PROPORTION of the thumbstick location
        //ie: Halfway all the way left means -0.5, halfway right means 0.5
        //so we need to find out how far we are from the center relative to the width of the thumbstick
        float proportionX = (float)relativeX/(float)((width-thumbstickCircleRadius)/2.0);
        float proportionY = (float)relativeY/(float)((height-thumbstickCircleRadius)/2.0);

        //now that we've figured out our how far from the center we are, proportionally speaking,
        //clamp it to a value between 1 and -1 for external application use
        valueX = clamp(proportionX, -1, 1);
        valueY = clamp(proportionY, -1, 1);

        //notify external listeners of the thumbstick moving
        onThumbstickMoved();
    }

    /**
     * Ensures that a number stays between a minimum and a maximum value
     * @param number
     * @param min
     * @param max
     * @return The clamped number
     */
    private  float clamp(float number, float min, float max) {
        if(number <= min)
            return min;
        else if(number >= max)
            return max;
        else
            return number;
    }

    public float getValueX() {
        return this.valueX;
    }

    public float getValueY() {
        return this.valueY;
    }

    /**
     * The X coord of the center of the thumbstick
     * @return
     */
    private int getCenterX() {
        return backgroundCircle.getBounds().centerX();
    }

    /**
     * The Y coord of the center of the thumbstick
     * @return
     */
    private int getCenterY() {
        return backgroundCircle.getBounds().centerY();
    }

    public void addThumbstickListener(ThumbstickListener l) {
        listeners.add(l);
    }

    public void removeThumbstickListener(ThumbstickListener l) {
        listeners.remove(l);
    }

    private void onThumbstickMoved() {
        for(ThumbstickListener l : listeners)
            l.thumbstickMoved(valueX,valueY);
    }

    public interface ThumbstickListener {
        void thumbstickMoved(float x, float y);
    }
}
